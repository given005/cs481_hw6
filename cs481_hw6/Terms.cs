﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace cs481_hw6
{ 
    //Class matches JSON content recieved from site data can be accesed acrodingly
    public class Definition
    {
        
        public string type { get; set; }
        
        public string definition { get; set; }
        
        public string example { get; set; }
        public object image_url { get; set; }
        public object emoji { get; set; }
    }

    public class Terms
    {
        public List<Definition> definitions { get; set; }
        public string word { get; set; }
        public string pronunciation { get; set; }
    }
    
}
