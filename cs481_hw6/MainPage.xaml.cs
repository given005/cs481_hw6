﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;

namespace cs481_hw6
{
    
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        RestService _restService;
        public MainPage()
        {
            InitializeComponent();
            _restService = new RestService();
            
           
        }
        //Checks for internet connection
        public bool DoIHaveInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }


        async void Button_Clicked(object sender, EventArgs e)
        {
            //When button clicked checks if whitespace is blank and if you have intertnet connection
           if(!string.IsNullOrWhiteSpace(word.Text) && DoIHaveInternet() == true)
           {
                Terms info = await _restService.GetInfo(GenerateRequestUri(Constants.dictionary));
                DictList.ItemsSource = info.definitions;
           }
        }

        //uploads Entry box data
        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"{word.Text}";
            
            
            return requestUri;
        }
    }
}
