﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel;
using Xamarin.Forms;
using System.Net.Http.Headers;

namespace cs481_hw6
{
    public class RestService
    {
        HttpClient _client;
        

        public RestService()
        {
            //Creates client and puts in authoriaztion token
            _client = new HttpClient();
           
            _client.DefaultRequestHeaders.Add("Authorization", "Token d9a8a6c4f89d555a626becbacb02a1ffd0beb644");
        }

       

        public async Task<Terms> GetInfo(string uri)
        {
            
            Terms info = null;
            //try attmepts to access site and deserailize the JSON data
            try
            {
                
                HttpResponseMessage response = await _client.GetAsync(uri);          
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    info = JsonConvert.DeserializeObject<Terms>(content);
                }
                
                
            }
            //Catch gives out error if problem with connection or JSON content
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return info;
        }


    }
}
